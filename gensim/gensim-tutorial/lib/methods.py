#!/usr/bin/env python3

# this work is based on a tutorial from Machine Learning Plus:
# https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/
# (My Bibtex ref: @SP18). The program works on a sample dataset of newsgroup
# postings, to pre-process the text, run it through an LDA topic model, produce
# various outputs and plot some of them in different ways.

# TODO: Add and improve comments. Number of sections are not explained. Plus in
# re-arranging code many of the existing comments are now outdated or
# unnecessary.

# Import all the packages needed for this program
import os # So I can test each section of my code, with exit()
import io
import sys
import re

import json
import pandas
from pprint import pprint

# Import gensim packages for LDA topic modelling
import gensim
import gensim.corpora as corpora

from gensim.utils import simple_preprocess
from gensim.models.phrases import Phrases, Phraser # building bigrams & trigrams

from gensim.models import CoherenceModel

# From nltk import stopwords library
from nltk.corpus import stopwords

# Import spacy for lemmatisation
import spacy

# Import plotting tools, to use in visualising data
import pyLDAvis
import pyLDAvis.gensim
import matplotlib.pyplot as plt

import logging
import warnings

warnings.filterwarnings("ignore",category=DeprecationWarning)

ALLOWED_POS_TAGS = ['NOUN', 'ADJ', 'VERB', 'ADV']


# 5. Prepare nltk stopwords, so they can be removed from text corpora. Also
# adding a few additional stopwords which should be removed.
from nltk.corpus import stopwords


# Define functions for stopwords, bigrams, trigrams and lemmatisation
def remove_stopwords(texts):
    stop_words = stopwords.words('english')
    stop_words.extend(['from', 'subject', 're', 'edu', 'use'])

    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]

def make_bigrams(texts):
    return [bigram_mod[doc] for doc in texts]

def make_trigrams(texts):
    return [trigram_mod[bigram_mod[doc]] for doc in texts]

def lemmatization(texts, allowed_postags=ALLOWED_POS_TAGS):
    """https://spacy.io/api/annotation"""
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append([token.lemma_ for token in doc if token. pos_ in allowed_postags])

    return texts_out

def generate_models(corpus, id2word, min_topic_count, max_topic_count):

    for n in range (min_topic_count, max_topic_count):
        print("\n>>> Generating " + str(n) + " topics <<<\n")

        # 12. Building the Topic Model
        # In addition to the corpus and dictionary, need to provide the number
        # of topics. alpha and eta are hyperparameters which affect the sparsity
        # of the topics. In gensim, both default to 1.0/num_topics prior.
        # `chunksize` is the number of documents to be used in each training
        # chunk. `update_every` determines how often the model parameters should
        # be updated. `passes` is the total number of training passes.

        lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                            id2word=id2word,
                            num_topics=n,
                            random_state=100,
                            update_every=1,
                            chunksize=100,
                            passes=10,
                            alpha='auto',
                            per_word_topics=True)

        model_list.append(model)

    return(model_list)


def find_maximally_coherent_model(models, lemmatised_data, dictionary, min_topic_count, max_topic_count):
    # Compute coherence score
    max_model = None
    max_coherence_model = None

    for n in range (min_topic_count, max_topic_count):
        coherence_model = CoherenceModel(model=models[n],
                texts=lemmatised_data,
                dictionary=dictionary,
                coherence='c_v')

        if(max_coherence_model == None or coherence_model.get_coherence() > max_coherence_model.get_coherence()):
            max_coherence_model = coherence_model.get_coherence()
            max_model = models[n]

    return(max_model)


def print_topic_keywords(models, min_topic_count, max_topic_count):
    for n in range (min_topic_count, max_topic_count):
        # Print the Keywords for each of the 10 tbopics
        print(">>> keywords for each topic <<<")
        pprint(lda_model[n].print_topics())


def format_topics_sentences(ldamodel=lda_model, corpus=corpus, texts=data):

        # Init output
        sent_topics_df = pandas.DataFrame()

        # Get main topic in each document
        for i, row in enumerate(ldamodel[corpus]):
            row = sorted(row, key=lambda x: (x[1]), reverse=True)

            # Get the dominant topic, perecentage contrinbution and keywords
            # for each document
            for j, (topic_num, prop_topic) in enumerate(row):
                if j == 0: # => dominant topic
                    wp = ldamodel.show_topic(topic_num)
                    topic_keywords = ", ".join([word for word, prop in wp])

                    sent_topics_df = sent_topics_df.append(pandas.Series([int
                                    (topic_num), round(prop_topic,4),
                                    topic_keywords]), ignore_index=True)
                else:
                    break
        sent_topics_df.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']

        # Add original text to the end of the output
        contents = pandas.Series(texts)
        sent_topics_df = pandas.concat([sent_topics_df, contents], axis=1)
        return(sent_topics_df)
