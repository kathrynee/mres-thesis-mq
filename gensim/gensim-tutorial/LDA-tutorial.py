#!/usr/bin/env python3

# This work is based on a tutorial from Machine Learning Plus:
# https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/
# (My Bibtex ref: @SP18). The program works on a sample dataset of newsgroup
# postings, to pre-process the text, run it through an LDA topic model, produce
# various outputs and plot some of them in different ways.

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!   Import all the packages needed for this program    !!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

import os # So I can test each section of my code, with exit()
import io
import sys
import re
import shutil

import json
import pandas
from pprint import pprint

import gensim # Gensim packages required for LDA topic modelling
import gensim.corpora as corpora

from gensim.utils import simple_preprocess
from gensim.models.phrases import Phrases, Phraser # building bigrams & trigrams
from gensim.models.coherencemodel import CoherenceModel
from pprint import pformat
from nltk.corpus import stopwords # stopwords library from nltk
import spacy # For lemmatisation

import pyLDAvis # Plotting tools, to use in visualising data
import pyLDAvis.gensim
import matplotlib.pyplot as plt

import logging
import warnings


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!         Setting constants, caching & logging           !!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Changed min and max topic count to a small range to speed up the processing
# time. Using 20-group newsgroup dataset which contains 11,000 posts. Dataset
# available from https://raw.githubusercontent.com/selva86/datasets/master/newsgroups.json')
# Information about the origin of this dataset is here: http://kdd.ics.uci.edu/databases/20newsgrou$
MIN_TOPIC_COUNT = 5
MAX_TOPIC_COUNT = 20

# MIN_BIGRAM_COUNT is the minimum number of times a token couple occurs before
# counted it is recorded as a bigram. Gensim's default is 5.
MIN_BIGRAM_COUNT = 5
BIGRAM_THRESHOLD = 2 # user defined: the strength of the relationship between two tokens required before the model accepts them as a phrase
BIGRAM_DELIMITER = b' ' # so the two bigram words remain separate

MIN_TRIGRAM_COUNT = MIN_BIGRAM_COUNT
TRIGRAM_THRESHOLD = BIGRAM_THRESHOLD
TRIGRAM_DELIMITER = b' '

PHRASES_COUNT = 5
PHRASES_THRESHOLD = 100
N_GRAM_DEBUG_LOGGING = True
DEBUG_OUTPUT_PATH = "debugOutput/"
# TODO: Change code to read the data file from the command line? ARGV?
DATASET_FILE = 'newsgroups-cleaned.json'
ALLOWED_POS_TAGS = ['NOUN', 'ADJ', 'VERB', 'ADV']

STOPWORDS = stopwords.words('english')
STOPWORDS.extend(['would', 'be', 're', 'edu', 'use', 'get', 'not', 'do', 'could', 'go'])

LDA_MODEL_RANDOM_STATE=100
LDA_MODEL_UPDATE_EVERY=1
LDA_MODEL_CHUNKSIZE=100
LDA_MODEL_PASSES=10
LDA_MODEL_ALPHA='auto'
LDA_MODEL_PER_WORD_TOPICS=True

# _DIRECTORY = "tmp/models"


if DEBUG_OUTPUT_PATH:
        shutil.rmtree(DEBUG_OUTPUT_PATH, ignore_errors=True)
        os.makedirs(DEBUG_OUTPUT_PATH+"/bigram", exist_ok=True)
        os.makedirs(DEBUG_OUTPUT_PATH+"/trigram", exist_ok=True)

# if os.path.exists(CACHE_DIRECTORY) == False:
#         os.makedirs(CACHE_DIRECTORY)
#         CACHED=False
# else:
#         CACHED=True


# Enable logging, set overall level and format of messages.
# TODO: At present these print to screen, change so prints to file
LOG_LEVEL = logging.DEBUG
LOG_FORMAT = '%(asctime)s --- %(levelname)5s: [%(filename)s:%(lineno)d] %(message)s'


logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)
warnings.filterwarnings("ignore", category=DeprecationWarning)


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!                  Defining functions                   !!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def tokenise_documents(documents):
    for document in documents:
        yield(gensim.utils.simple_preprocess(str(document), deacc=True))


# !!!! Import the dataset, convert to a list, clean and produce data corpus !!!!
def make_corpus(dataset_file):
    pandas_dataframe = pandas.read_json(dataset_file) # convert the json formatted dataset to a pandas dataframe

    # Logging for the import of dataset and checking data.
    logging.info("Import Dataset: {0}".format(DATASET_FILE))
    logging.debug("Newsgroups: {0}s".format(", ".join(pandas_dataframe.target_names.unique())))
    logging.debug("First five datafile items:\n{0}".format(pandas_dataframe.head(5)))

    data = pandas_dataframe.content.values.tolist() # convert pandas dataframe to a list

    data_corpus_of_words = list(tokenise_documents(data))

    logging.debug("First document (words): {0}".format(format(data[:1][0])))
    logging.debug("First document (tokenised): {0}".format(format(data_corpus_of_words[:1][0])))

    return data_corpus_of_words


# !!!! Remove stopwords !!!!
def remove_stopwords(doc):
        output = []
        for line in doc:
            document_words = simple_preprocess(str(line))
            document_words_no_stopwords = [word for word in document_words if word not in STOPWORDS]
            output.append(document_words_no_stopwords)
            stopwords_in_document = [word for word in document_words if word in STOPWORDS]
            logging.debug("Stop words in single document: " + str(set(document_words) - set(document_words_no_stopwords)))
            logging.debug("Document length {}, number of stopwords {}, returned line length {}".format(len(line), len(stopwords_in_document), len(document_words_no_stopwords)  ))
            # logging.debug("Returning {}".format(document_words_no_stopwords))
            return output;


# !!!! Find bigrams and trigrams in the corpus of text !!!!
# !!!! This tutorial helped in understanding bigrams / trigrams: https://towardsdatascience.com/another-twitter-sentiment-analysis-with-python-part-7-phrase-modeling-doc2vec-592a8a996867 !!!!
def make_bigrams_and_trigrams(texts):

        bigram_phrases = gensim.models.Phrases(texts, min_count=MIN_BIGRAM_COUNT, threshold=BIGRAM_THRESHOLD, delimiter=BIGRAM_DELIMITER)
        bigram_phraser = gensim.models.phrases.Phraser(bigram_phrases)
        trigram_phrases  = gensim.models.Phrases(bigram_phrases[texts], min_count=MIN_TRIGRAM_COUNT, threshold=TRIGRAM_THRESHOLD, delimiter=TRIGRAM_DELIMITER)
        trigram_phraser = gensim.models.phrases.Phraser(trigram_phrases)


        trigram_list = []
        print(texts[:1])
        print(len(texts))
        for i, sentence in enumerate(texts):
            bigram = bigram_phraser[sentence]
            trigram = trigram_phraser[bigram]
            trigram_list.append(trigram)

        if N_GRAM_DEBUG_LOGGING:
            with open("{}/trigram/trigram_{}.json".format(DEBUG_OUTPUT_PATH, i),mode="w") as debugOutputFile:
                json.dump(trigram, debugOutputFile)
                # logging.info("Number of bigrams made: {}".format(len(bigram_list)))
                logging.info("Number of trigrams made: {}".format(len(trigram_list)))

                return trigram_list


# !!!!!!!    Lemmatise the text      !!!!!!!
# Removes inflectional endings forms of a word, converting the corpus to the
# base or dictionary form of the word, known as the lemma. Notes on spaCy's
# lemmatisation are here: https://spacy.io/api/annotation
def lemmatization(texts, allowed_postags=ALLOWED_POS_TAGS):
        nlp = spacy.load('en', disable=['parser', 'ner']) # Initialise spacy 'en' model.

        texts_out = []
        for sent in texts:
                doc = nlp(" ".join(sent))
                texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
                # logging.debug("This is the lemmatised text: {}".format(texts_out))
        return texts_out





# !!!!!!!    Build, load & save the topic model   !!!!!!!

def generate_models(corpus, id2word, min_topic_count, max_topic_count):
        model_list = []

        for n in range (min_topic_count, max_topic_count):
                logging.debug("Generating topic {0}d of {1}d.".format(n, max_topic_count))

                # Information on these options: https://radimrehurek.com/gensim/models/ldamodel.html
                model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                id2word=id2word,
                                num_topics=n,
                                random_state=LDA_MODEL_RANDOM_STATE,
                                update_every=LDA_MODEL_UPDATE_EVERY,
                                chunksize=LDA_MODEL_CHUNKSIZE,
                                passes=LDA_MODEL_PASSES,
                                alpha=LDA_MODEL_ALPHA,
                                per_word_topics=LDA_MODEL_PER_WORD_TOPICS)


                model_list.append(model)

        return(model_list)



def save_models(models, directory, min_topic_number):
        """ Utility method to save a model. """
        for n in range(len(models)):
            models[n].save(directory + "/" + str(n))



def load_models(directory, n):
        models = []
        """ Utility method to load model from disc. """
        for n in range(n):
                models.append(gensim.models.ldamodel.LdaModel.load(directory + "/" + str(n)))
        return(models)



# !!!!  Method to choose topic with the higest coherence score.   !!!!
# !!!!  The coherence score is a good indicator of topic quality  !!!!

# A useful explanation of the Coherence Score can be found here: http://web.archive.org/web/20160920054805/http://rare-technologies.com:80/what-is-topic-coherence/
def find_maximally_coherent_model(models, lemmatised_data, dictionary, min_topic_number):

        max_model = None
        max_n = 0
        max_coherence_model = None
        for n in range(len(models)): # Documentation for these parameters: https://radimrehurek.com/gensim/models/coherencemodel.html
                coherence_model = CoherenceModel(model=models[n],
                                texts=lemmatised_data,
                                dictionary=dictionary,
                                coherence='c_v')

                if(max_coherence_model == None or coherence_model.get_coherence() > max_coherence_model.get_coherence()):
                        max_coherence_model = coherence_model
                        max_n = n + min_topic_number
                        max_model = models[n]



        return((max_n, max_model))



# !!!!!!!    Keywords for the topics    !!!!!!!

# Keywords for the topic number with the highest Topic Coherence
def log_topic_n_keywords(model, topic_number):
        """ Log keywords for the Nth model. """
        logging.debug("keywords for topic: " + str(topic_number))
        logging.debug("{}".format(model.print_topics()))

# Keywords for all the topics
def log_topic_keywords(models, min_topic_count, max_topic_count):
        """ Log keywords for the all models. """
        logging.debug("All topics:")
        for n in range(len(models)):
                log_topic_n_keywords(models[n], min_topic_count + n)



# !!!!!!!    Find the dominant topic in each sentence    !!!!!!!
def format_topics_sentences(ldamodel, corpus, texts):
        top_10_array = [] # Array of top 10 topics

        for row in range(ldamodel.num_topics):
                word_probability = ldamodel.show_topic(row)
                topic_keywords = ",".join([word for word, prop in word_probability])
                top_10_array.append((row+1, topic_keywords))

        top_10_topic_dict = dict(top_10_array)

        sent_topics_pandas_dataframe = pandas.DataFrame() # Init output

        logging.debug("top_10_topic_dict: {0}".format(top_10_topic_dict))

        for topic in (ldamodel[corpus]): # Get main topic in each document
                row = topic[0]
                row = sorted(row, key=lambda x: (x[0]), reverse=True)

                # Get the dominant topic, percentage contribution and keywords for each document
                for j, (topic_num, prop_topic) in enumerate(row):
                        if j == 0:  # dominant topic
                                topic_num +=1 #view topics as a series starting from 1
                                topic_keywords = top_10_topic_dict[topic_num]
                                sent_topics_pandas_dataframe = sent_topics_pandas_dataframe.append(pandas.Series([int (topic_num), round(prop_topic,4), topic_keywords]), ignore_index=True)
                        else:
                                break

        # displaying the topic and sentence in a table.
        sent_topics_pandas_dataframe.columns = ['Dominant_Topic', 'Perc_Contribution', 'Topic_Keywords']

        contents = pandas.Series(texts) # Add original text to the end of the output
        sent_topics_pandas_dataframe = pandas.concat([sent_topics_pandas_dataframe, contents], axis=1)
        return(sent_topics_pandas_dataframe)


# !!!!!!!    Output topic number and keywords    !!!!!!!

# Outputs the number of topics formatted as json.
# An example of the output format is:
# {"topics":{
#   "<topic number>":[{"<keyword>": <value>}, ...]
#   }
# }
#
def output_topics(ldamodel, corpus, fd=sys.stdout, number_of_topics=10):
        top_topics = {}

        print("####################################################")
        print("number of topics: " + str(number_of_topics))

        for topic_number in range(ldamodel.num_topics):
                word_probability = ldamodel.show_topic(topic_number)
                top_topics[topic_number] = [word for word, probablity in word_probability]

        json.dump({"topics": top_topics}, fd)



# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!           The actual work starts here                 !!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


corpus_pre_stopwords = make_corpus(DATASET_FILE)
#logging.debug(len(corpus_pre_stopwords))

document_words_no_stopwords = remove_stopwords(corpus_pre_stopwords)
#logging.debug(len(document_words_no_stopwords))

logging.debug("stopwords: " + str(STOPWORDS))
# if (CACHED):
#    logging.info("Loading models from disc")
#    models = load_models(CACHE_DIRECTORY, MAX_TOPIC_COUNT - MIN_TOPIC_COUNT - 1)

#    logging.debug("First document (Document Without Stopwords): {0}".format(document_words_no_stopwords[:1][0]))

data_words_n_gram = make_bigrams_and_trigrams(document_words_no_stopwords)

data_lemmatized = lemmatization(data_words_n_gram, allowed_postags=ALLOWED_POS_TAGS) # Lemmatise the text

#logging.debug("First document (lemmatised): {0}".format(pformat(data_lemmatized[:1][0])))

id2word = corpora.Dictionary(data_lemmatized) # Create dictionary

corpus_Term_Document_Frequency = [id2word.doc2bow(text) for text in data_lemmatized] # Term frequency

print(">>> unique id for each word in corpus <<<")
print(corpus_Term_Document_Frequency[:1])

logging.debug("Term frequencies.")

for n in range(len(corpus_Term_Document_Frequency[:2])):
        term_and_freq = [(id2word[id], freq) for id, freq in corpus_Term_Document_Frequency[:2][n]]
        logging.debug("Term frequency for document: {0}: {1}".format(n, term_and_freq))

topic_models = generate_models(corpus_Term_Document_Frequency, id2word, MIN_TOPIC_COUNT, MAX_TOPIC_COUNT)

# save_models(topic_models, CACHE_DIRECTORY, MIN_TOPIC_COUNT)



# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!       Finding the optimal number of topics            !!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

(topic_number, maximally_coherent_model) = find_maximally_coherent_model(topic_models, data_lemmatized, id2word, MIN_TOPIC_COUNT)

logging.debug("{0}d {1}".format(topic_number, maximally_coherent_model))

log_topic_keywords(topic_models, MIN_TOPIC_COUNT, MAX_TOPIC_COUNT)

logging.info("Maximally coherent topic: {0}".format(topic_number))
#log_topic_keywords(maximally_coherent_model, topic_number)

t = MAX_TOPIC_COUNT - MIN_TOPIC_COUNT - 1

output_topics(maximally_coherent_model, corpus_Term_Document_Frequency, number_of_topics=t)

pandas_dataframe_topic_sents_keywords = format_topics_sentences(maximally_coherent_model, corpus_Term_Document_Frequency, document_words_no_stopwords)

logging.info("Topic sentence keywords:\n{0}".format(pandas_dataframe_topic_sents_keywords))

pandas_dataframe_dominant_topic = pandas_dataframe_topic_sents_keywords.reset_index()

pandas_dataframe_dominant_topic.columns = ['Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text'] # Check these columns

logging.info("Dominant topic\n{0}".format(pandas_dataframe_dominant_topic.head(10)))


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!   Finding the most representative documents for each topics   !!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

main_sent_topics = pandas.DataFrame()

main_sent_outpandas_dataframe_grouped = pandas_dataframe_topic_sents_keywords.groupby('Dominant_Topic')

for i, grp in main_sent_outpandas_dataframe_grouped:
            main_sent_topics = pandas.concat([main_sent_topics,
                    grp.sort_values(['Perc_Contribution'],
                            ascending=[0]).head(1)], axis=0)

main_sent_topics.reset_index(drop=True, inplace=True)

main_sent_topics.columns = ['Topic_Num', "Topic_Perc_Contrib", "Keywords", "Text"]

main_sent_topics.head()



# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!           Topic distribution across documents           !!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

topic_counts = pandas_dataframe_topic_sents_keywords['Dominant_Topic'].value_counts() # Number of documents for each topic

topic_contribution = round(topic_counts/topic_counts.sum(), 4) # Percentage of documents for each topic

topic_contribution = round(topic_counts/topic_counts.sum(), 4) # Percentage of documents for each topic

topic_num_keywords = pandas_dataframe_topic_sents_keywords[['Dominant_Topic', 'Topic_Keywords']] # Topic number and keywords

pandas_dataframe_dominant_topics = pandas.concat([topic_num_keywords, topic_counts, topic_contribution], axis=1) # Concantenate columns

pandas_dataframe_dominant_topics.columns = ['Topic_Number_Keywords', 'Dominant_Topic', 'Num_Documents', 'Perc_Documents']

pandas_dataframe_dominant_topics



logging.info("TFI turnips!")
