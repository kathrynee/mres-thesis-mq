from pprint import pprint

from sys import exit
from pathlib import Path
from datetime import datetime
import subprocess as subprocess

import csv
import time
import json
import hashlib

import spacy
from spacy.tokens import Doc

import gensim
from gensim.utils import simple_preprocess
from gensim.corpora import Dictionary
from gensim.models.tfidfmodel import TfidfModel
from gensim.models.ldamodel import LdaModel
from gensim.models.ldamulticore import LdaMulticore
from gensim.models.phrases import Phrases, Phraser
from gensim.models.coherencemodel import CoherenceModel

from nltk.corpus import stopwords # stopwords library from nltk

import pyLDAvis
import pyLDAvis.gensim
import matplotlib.pyplot as plt

from pprint import pformat

import logging
import warnings

warnings.filterwarnings("ignore") #, category=DeprecationWarning)

LOG_LEVEL = logging.DEBUG
LOG_FORMAT = '%(asctime)s --- %(levelname)5s: [%(filename)s:%(lineno)d] %(message)s'

DEFAULT_POS_TAGS = ['NOUN', 'ADJ', 'VERB', 'ADV']

N_GRAM_OPTIONS = dict(min_count=5, threshold=2, delimiter=b' ')
LDA_OPTIONS = dict(random_state=100, update_every=0, chunksize=2000,
                   passes=20, alpha='auto', eta='auto',
                   per_word_topics=True, eval_every=1,
                   iterations=400)


class TopicModel:
    # Initialise the topic model class.
    #
    # filename            -> The name of the JSON containing the corpus data
    # min_topic_count     -> The smallest number of topics to search for while optimising for the number of topics
    # max_topic_count     -> The maximum number of topics to search for while optimising for the number of topics
    # topic_step          -> The step in topic range, defaults to 1
    # pos_tags            -> Parts of Speech to used for processing. Anything not in this list will be ignored
    # tfidf               -> Optionally process the corpus using tfidf
    # trigrams            -> Optionally generate tri-grams
    # bigrams             -> Optionally generate bi-grams
    # log_level           -> The level at which to log
    def __init__(self, filename, min_topic_count=1, max_topic_count=20, topic_step=1, tfidf=True, pos_tags=DEFAULT_POS_TAGS,
                       log_to_console=True, log_level=logging.DEBUG, bigrams=False, trigrams=False):

        current_time = datetime.now()
        self._unique_run_id = str(current_time.strftime("%s.%f"))

        self._persistance = TopicPersistance(base_dir = "/tmp/LDA/{0}".format(self._unique_run_id))

        if log_to_console:
            logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)
        else:
            log_file = "/tmp/LDA/{0}/topic-model.log".format(self._unique_run_id)
            print("Saving log file to: " + log_file)
            logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL, filename=log_file)


        (self._json, file_hash) = self._load_hasd_and_decode(filename)

        self._min_topic_count   = min_topic_count
        self._max_topic_count   = max_topic_count
        self._topic_step        = topic_step
        self._pos_tags          = pos_tags
        self._tfidf             = tfidf
        self._bigrams           = bigrams
        self._trigrams          = trigrams


        self._persistance.save_params(json.dumps(dict(input_filename=filename, input_file_sha256=file_hash,
                                                      min_topic_count=min_topic_count, max_topic_count=max_topic_count,
                                                      topic_step=topic_step, tfidf=tfidf, trigrams=trigrams, bigrams=bigrams,
                                                      pos_tags=pos_tags, timestamp=current_time.isoformat(), git=self.repo_info())))

        self._logging = logging.getLogger(str(self.__class__))

        self._logging.info("Setting up stopwords.")

        self._stopwords = stopwords.words('english')
        self._stopwords.extend(['woolworths', 'coles'])

        self._nlp = spacy.load('en', disable=['parser', 'ner'])

        # This is an Array of documents (which themselves are Arrays).
        self._corpus = []
        self._corpus_size = -1


    def pre_process_corpus(self):
        """ Pre-process each document in the corpus
        """
        self._logging.info("Tokenising corpus.")
        tokenised_corpus = [self._tokenise(doc)  for doc in self._json]

        self._logging.info("Loaded {0} documents.".format(len(tokenised_corpus)))

        self._logging.info("Removing stopwords, lemmatising & building the corpus")
        self._logging.info("Keeping lemmatised POS tags: {0}".format(str(self._pos_tags)))

        for doc_tokens in tokenised_corpus:
            self._corpus.append(self._lemmatise(self._remove_stopwords(doc_tokens)))

        if self._bigrams or self._trigrams:
            (bigrams, trigrams) = self._build_ngrams(self._corpus)

            if self._bigrams:
                self._logging.info("Appending bi-grams to the dictionary as specified.")
                for n in range(len(self._corpus)):
                    for token in bigrams[self._corpus[n]]:
                        self._corpus[n].append(token)


            if self._trigrams:
                self._logging.info("Appending tri-grams to the dictionary as specified.")
                for n in range(len(self._corpus)):
                    for token in trigrams[self._corpus[n]]:
                        self._corpus[n].append(token)

        else:
            self._logging.info("Skipping tri-gram processing.")

        self._corpus_size = len(tokenised_corpus)


    def _load_hasd_and_decode(self, filename):
        with open(filename) as fd:
            data = fd.read()
            content = json.loads(data)["content"]

            n = 0
            self._document_docid_map = []

            for key in content.keys():
                self._document_docid_map.append((n, key))
                n += 1

            return (content.values(), self.hash_input_data(data))


    def corpus(self):
        """ Return the corpus.
            return -- [[String]]
        """
        return self._corpus


    # Tokenise a single document
    def _tokenise(self, doc):
        """ Tokenise a single document.
            doc -- String
            return -- [String]
        """
        return simple_preprocess(str(doc), deacc=True)


    # Remove stopwords for a single document
    # doc -> the document as a string
    def _remove_stopwords(self, doc):
        """ Remove stopwords from a single document.
            doc -- [String]
            return -- [String]
        """
        return [word for word in doc if word not in self._stopwords]


    def _lemmatise(self, doc):
        """ Lemmatise all words in a single document.
            doc -- [String]
            return -- [String]
        """
        doc_ = self._nlp(" ".join(doc))
        return [token.lemma_ for token in doc_ if token.pos_ in self._pos_tags]


    def _trigrams(self, doc):
        """ Generate tri-grams for each document. The resultant output are a
            concatenation of each tri-gram into a single token using an
            underscore. For example: "the cat sat on the mat" would be:
            "the_cat_sat, cat_sat_on ..." -- although this may be wrong.
            doc -- [String]
            return -- [String]
        """
        trigram = self._trigrams[self._bigrams[doc]]
        return trigram


    # Generates tri-grams from the tokenised corpus
    def _build_ngrams(self, tokenised_corpus):
        self._logging.info("Building bi-grams")
        bigram_phrases = Phrases(tokenised_corpus, **N_GRAM_OPTIONS)

        self._logging.info("Building tri-grams")
        trigram_phrases = Phraser(Phrases(bigram_phrases[tokenised_corpus], **N_GRAM_OPTIONS))

        return (bigram_phrases, trigram_phrases)


    def run_lda(self):
        self.pre_process_corpus()
        self._build_dictionary()
        self._build_bow()
        self._build_tfidf()
        self._build_models()


    # BOW (bag of words) dictionary of number of words and how many times those words appear
    def _build_bow(self):
        self._logging.info("Building the BOW representation of the corpus")
        self._bow = [self._dictionary.doc2bow(doc) for doc in self._corpus]


    def _build_tfidf(self):
        if self._tfidf:
            self._logging.info("Building TF-IDF model as specified.")
            self._tfidf_corpus = TfidfModel(self._bow, id2word=self._dictionary)
            self._foo = self._tfidf_corpus[self._bow]
        else:
            self._logging.info("Skipping TF-IDF processing.")
            self._foo = self._bow


    def _build_dictionary(self, tfidf=True, compactify=True):
        self._logging.info("Building Dictionary.")
        self._dictionary = Dictionary(self._corpus)

        self._logging.info("Dictionary Stats - num of documents: {0}".format(self._dictionary.num_docs))
        self._logging.info("Dictionary Stats - num of tokens: {0}".format(self._dictionary.num_pos))


        if compactify:
            # Ignore words that appear in less than 50 documents or more than 20% documents
            # self._dictionary.filter_extremes(no_below=50, no_above=0.2)
            self._dictionary.compactify()


    def _topics_generator(self):
        for n in range (self._min_topic_count, self._max_topic_count, self._topic_step):
            yield (n, self._max_topic_count)


    def _build_models(self, quiet=True):
        models = {}

        for topic_number, max_topic in self._topics_generator():
            self._logging.info("Building topic {0} of {1}.".format(topic_number, max_topic))
            models[topic_number] = LdaModel(corpus=self._foo, id2word=self._dictionary, num_topics=topic_number, **LDA_OPTIONS)

        self._models = models


    def find_maximally_coherent_model(self):
        max_model = None
        max_n = 0
        max_coherence_model = None
        scores = []

        for topic_number, max_topic in self._topics_generator():
            coherence_model = self._coherence_models[topic_number]
            coherence_score = coherence_model.get_coherence()

            scores.append((topic_number, coherence_score))

            if(max_coherence_model == None or coherence_score > max_coherence_model.get_coherence()):
                max_coherence_model = coherence_model
                max_n = topic_number
                max_model = self._models[topic_number]

        for (n, score) in scores:
            self._logging.info("Coherence for topic {0} = {1}".format(n, score))

        return((max_n, max_model))


    def calculate_coherence_models(self):
        self._coherence_models = {}

        # Documentation for these parameters: https://radimrehurek.com/gensim/models/coherencemodel.html
        for topic_number, max_topic in self._topics_generator():
            self._logging.info("Calulating coherence model for {0}.".format(topic_number))
            self._coherence_models[topic_number] = CoherenceModel(model = self._models[topic_number], texts = self._corpus, dictionary = self._dictionary, coherence='c_v')
            self._logging.info("Topic coherence: {0}".format(self._coherence_models[topic_number].get_coherence()))


    def export_topics_per_documents(self, model_id):
        fields = ["run", "model_id", "topic_id", "document_id", "probability"]

        model = self._models[model_id]

        path = self._persistance.csv_file_path(model_id)
        self._logging.info("Exporting documents for topic {0} to: {1}".format(model_id, path))

        with open(str(path), 'w', newline='') as  csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fields, delimiter="\t")
            writer.writeheader()

            for document_id in range(self._corpus_size):
                topic_probability_pairs = model.get_document_topics(self._bow[document_id])

                for (topic_id, probability) in topic_probability_pairs:
                    writer.writerow({"run": self._unique_run_id, "model_id": model_id, "topic_id": topic_id, "document_id": document_id, "probability": probability})


    def export_all_topics_per_documents(self):
        self._logging.info("Exporting documents for all topics.")

        for topic_id, max_topic in self._topics_generator():
            self.export_topics_per_documents(topic_id)


    def export_document_id_map(self):
        fields = ["run", "document_id", "document_name"]

        path = self._persistance.document_map_csv_file_path()
        self._logging.info("Exporting document to document_id map")

        with open(path, 'w', newline='') as  csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fields, delimiter="\t")
            writer.writeheader()

            for (k, v) in self._document_docid_map:
                writer.writerow({"run": self._unique_run_id, "document_id": k, "document_name": v})


    def print_topics_per_documents(self, topic_id):
        model = self._models[topic_id]

        for n in range(self._corpus_size):
            print(model.get_document_topics(self._bow[n]))
            print()

    def print_topics(self, topn=20, extended=False):
        """ Print the top terms for each topic.
        """
        for topic_id, max_topic in self._topics_generator():
            self.print_topic(topic_id, topn=topn, extended=extended)


    def print_topic(self, topic_id, topn=20, extended=False):
        """ Print the top terms for all topics.
        """
        for topic in self._models[topic_id].show_topics(topn):
            self._logging.info(topic)

        if extended == True:
            for n in range(topic_id):
                self._logging.info("Terms per topic {0} (of {1})".format(n, topic_id - 1))
                self._logging.info("{:20} {}".format(u'term', u'frequency'))

                for term, frequency in self._models[topic_id].show_topic(n, topn):
                    self._logging.info("{:20} {:.3f}".format(term, round(frequency, 3)))

                logging.info("")


    def export_all_topic_visualisation_data(self):
        self._logging.info("Exporting topic visualisations for all topics.")

        for topic_id, max_topic in self._topics_generator():
            self.export_topic_visualisation_data(topic_id)


    def export_topic_visualisation_data(self, topic_id):
        model = self._models[topic_id]

        vis = pyLDAvis.gensim.prepare(model, self._bow, self._dictionary)
        self._persistance.save_visualisation(vis, topic_id)


    def repo_info(self):
        return subprocess.check_output(["git", "describe", "--always"]).decode("utf-8").strip()


    def hash_input_data(self, data):
        logging.info("Hashing the input file.")

        return hashlib.sha224(data.encode("utf-8")).hexdigest()



# Simple class to hide the details of saving data to the filesystem.
class TopicPersistance:

    TOPICS_DATA_DIR = "/tmp/LDA/"


    def __init__(self, base_dir=TOPICS_DATA_DIR):
        self._base_dir = Path(base_dir)
        self._mkdir(self._base_dir)


    def _mk_model_part(self, model_id):
        path = self._base_dir.joinpath(str(model_id))
        self._mkdir(path)
        return path


    def _mk_part(self, model_id, part, dir=False):
        path = self._base_dir.joinpath(str(model_id))
        if dir:
            d =  path.joinpath(part)
            self._mkdir(d)
            return d
        else:
            self._mkdir(path)
            return path.joinpath(part)


    def _mk_path(self, model_id, part):
        return self._base_dir.joinpath(str(model_id)).joinpath(part)


    def _mkdir(self, path):
        path.mkdir(mode=0o700, exist_ok=True, parents=True)


    def save_params(self, args):
        path = str(self._base_dir.joinpath("meta.json"))
        with open(path, "w") as fd:
            fd.write(str(args))


    def save_model(self, model, model_id):
        path = str(self._mk_part("corpus", "model"))
        logging.info("Saving the model to: " + path)
        model.save(path)


    def save_visualisation(self, vis, model_id):
        path = self._mk_model_part(model_id).joinpath("visualisation.html")
        logging.info("Exporting html visualisation for topic {0} to: {1}".format(model_id, path))
        pyLDAvis.save_html(vis, str(path))

    def csv_file_path(self, model_id):
        return self._mk_part(model_id, "topic-documents.csv")


    def document_map_csv_file_path(self):
        return str(self._base_dir.joinpath("document-document-id.csv"))
