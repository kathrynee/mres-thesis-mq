#!/usr/bin/env python3

import json
import argparse
from pprint import pprint

DATASET_FILE = './data/input/newsgroups.100.json'

parser = argparse.ArgumentParser(description='View sample data.')
parser.add_argument('--file', type=argparse.FileType('r'), default=DATASET_FILE)
parser.add_argument('--post', type=int, default=0)

args = parser.parse_args()
print(args)
with args.file as foo:
	newsgroups = json.load(foo)
	#your data is stupid and should be ashamed of itself.
	pprint(newsgroups["content"]["{}".format(args.post)])
	#pprint(newsgroups["target"][args.post])
	#pprint(newsgroups["target_names"][args.post])
