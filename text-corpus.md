Both Coles and Woolworths release either eleven or twelve editions of their
magazine each year. The Woolworths website has magazines available from 2009
although, there there are a number of issues missing from 2013 and 2014, where
the website produces an error message when I tried to download them. The Coles
website has magazines available from 2014. In total my data corpus comprises
103 issues of Woolworths' Fresh, from January 2009 to July 2018 and 49 issues
of Coles Magazine, dated from January / February 2014 -- June 2018, a total of
152 magazines. Following is a list of all the magazine editions in my corpus,
together with the URLs showing where the magazines can be found.

To make it simpler to trace the Coles and Woolworths magazine issues back to the original source, I have included the URL of each issue in the two tables in this appendix. In order to make the tables easier to read I have included the base domain name of each supermarkets' magazines at the top of the table. And then put the the particular URL path of each issue in the table itself.

## Coles Magazine

* Base URL: http://d2g5na3xotdfpc.cloudfront.net/uploads/pdf/

### 2014

|   Month     | Original Filename                                                  | Base Path         |
|-------------|--------------------------------------------------------------------|-------------------|
| January / February  | Coles - January_February 2014 - Offer valid Wed 1 Jan - Sat 7 Jun 2025.pdf | 1401_Colesmag.pdf |
| March               | Coles - March 2014 - Offer valid Sat 1 Mar - Sat 7 Jun 2025.pdf | 1403_Colesmag.pdf |
| April               | Coles - April 2014 - Offer valid Tue 1 Apr - Sat 7 Jun 2025.pdf | 1404_Colesmag.pdf |
| May                 | Coles - May 2014 - Offer valid Thu 1 May - Sat 7 Jun 2025.pdf | 1405_Colesmag.pdf |
| June                | Coles - June 2014 - Offer valid Sun 1 Jun - Sat 7 Jun 2025.pdf | 1406_Colesmag.pdf |
| July                | Coles - July 2014 - Offer valid Tue 1 Jul - Sat 7 Jun 2025.pdf | 1407_Colesmag.pdf |
| August              | Coles - August 2014 - Offer valid Fri 1 Aug - Sat 7 Jun 2025.pdf | 1408_Colesmag.pdf |
| September/October   | Coles - September_October 2014 - Offer valid Mon 1 Sep - Sat 7 Jun 2025.pdf | 1409_Colesmag.pdf |
| November            | Coles - November 2014 - Offer valid Sat 1 Nov - Sat 7 Jun 2025.pdf | 1411_Colesmag.pdf |
| December            | Coles - December 2014 - Offer valid Mon 1 Dec - Sat 7 Jun 2025.pdf | 1412_Colesmag.pdf |

### 2015

|   Month     | Original Filename                                                  | Base Path         |
|-------------|--------------------------------------------------------------------|-------------------|
| January/February | Coles - January_February 2015 - Offer valid Thu 1 Jan - Sat 7 Jun 2025.pdf | 1501_Colesmag.pdf |
| March       | Coles - March 2015 - Offer valid Sun 1 Mar - Sat 7 Jun 2025.pdf | 1503_Colesmag.pdf |
| April       | Coles - April 2015 - Offer valid Wed 1 Apr - Sat 7 Jun 2025.pdf | 1504_Colesmag.pdf |
| May         | Coles - May 2015 - Offer valid Fri 1 May - Sat 7 Jun 2025.pdf | 1505_Colesmag.pdf |
| June        | Coles - June 2015 - Offer valid Mon 1 Jun - Sat 7 Jun 2025.pdf | 1506_Colesmag.pdf |
| July        | Coles - July 2015 - Offer valid Wed 1 Jul - Sat 7 Jun 2025.pdf | 1507_Colesmag.pdf |
| August      | Coles - August 2015 - Offer valid Sat 1 Aug - Sat 7 Jun 2025.pdf | 1508_Colesmag.pdf |
| September   | Coles - September 2015 - Offer valid Tue 1 Sep - Sat 7 Jun 2025.pdf | 1509_Colesmag.pdf |
| October     | Coles - October 2015 - Offer valid Thu 1 Oct - Sat 7 Jun 2025.pdf | 1510_Colesmag.pdf |
| November    | Coles - November 2015 - Offer valid Sun 1 Nov - Sat 7 Jun 2025.pdf | 1511_Colesmag.pdf |
| December    | Coles - December 2015 - Offer valid Tue 1 Dec - Sat 7 Jun 2025.pdf | 1512_Colesmag.pdf |

### 2016

|   Month     | Original Filename                                                  | Base Path         |
|-------------|--------------------------------------------------------------------|-------------------|
| January / February   | Coles - January_February 2016 - Offer valid Fri 1 Jan - Sat 7 Jun 2025.pdf | 1601_Colesmag.pdf |
| March       | Coles - March 2016 - Offer valid Tue 1 Mar - Sat 7 Jun 2025.pdf | 1603_Colesmag.pdf |
| April       | Coles - April 2016 - Offer valid Fri 1 Apr - Sat 7 Jun 2025.pdf | 1604_Colesmag.pdf |
| May         | Coles - May 2016 - Offer valid Sun 1 May - Wed 31 Dec 2025.pdf | COLESMAGMAY2016.pdf |
| June        | Coles - June 2016 - Offer valid Wed 1 Jun - Fri 31 Jan 2025.pdf | 1606_Colesmag.pdf |
| July        | Coles - July 2016 - Offer valid Thu 7 Jul - Wed 31 Dec 2025.pdf | 1607_Colesmag.pdf |
| August      | Coles - August 2016 - Offer valid Thu 4 Aug - Mon 31 Dec 2035.pdf | Colesmag_Aug_2016.pdf |
| September   | Coles - September 2016 - Offer valid Thu 1 Sep - Tue 31 Dec 2030.pdf | Coles%20mag_Sept2016_salesfinder.pdf |
| October     | Coles - October 2016 - Offer valid Thu 6 Oct - Tue 31 Dec 2030.pdf | Coles%20mag_Oct%2016.pdf |
| November    | Coles - November 2016 - Offer valid Thu 3 Nov - Thu 31 Dec 2020.pdf | ColesMag_Nov16.pdf |
| December    | Coles - December 2016 - Offer valid Thu 1 Dec - Sun 1 Dec 2030.pdf | ColesMag_Dec16.pdf |

### 2017

|   Month     | Original Filename                                                  | Base Path         |
|-------------|--------------------------------------------------------------------|-------------------|
| February    | Coles - February 2017 - Offer valid Thu 26 Jan - Tue 31 Dec 2030.pdf | Coles%20mag%20Feb17_d83hk.pdf |
| March       | Coles - March 2017 - Offer valid Thu 2 Mar - Tue 31 Dec 2030.pdf | Coles_Mag_March2017_j8s3h.pdf |
| April       | Coles - April 2017 - Offer valid Thu 6 Apr - Tue 31 Dec 2030.pdf | Coles_Mag_April_2017_eh3ks.pdf |
| May         | Coles - May 2017 - Offer valid Thu 4 May - Tue 31 Dec 2030.pdf | Coles%20mag%20May%202017_dfj3ksls.pdf |
| June        | Coles - June 2017 - Offer valid Thu 1 Jun - Tue 31 Dec 2030.pdf | Coles%20mag%20June%202017_l20sf5r.pdf |
| July        | Coles - July 2017 - Offer valid Thu 6 Jul - Tue 31 Dec 2030.pdf | Coles%20mag%20July%202017_k8h6d.pdf |
| August      | Coles - August 2017 - Offer valid Thu 3 Aug - Tue 31 Dec 2030.pdf | Coles%20mag%20August%202017_k8u7y3h.pdf |
| September   | Coles - September 2017 - Offer valid Thu 7 Sep - Tue 31 Dec 2030.pdf | ColesMag_September2017_9k73jw3.pdf |
| October     | Coles - October 2017 - Offer valid Thu 5 Oct - Tue 31 Dec 2030.pdf | Coles%20Mag%20October%202017_83jdw.pdf |
| November    | Coles - November 2017 - Offer valid Mon 6 Nov - Tue 31 Dec 2030.pdf | Coles_Nov%20Mag_2017_7d93k.pdf |
| December    | Coles - December 2017 - Offer valid Thu 7 Dec - Tue 31 Dec 2030.pdf | Coles_Dec2017%20Mag_h8k39i.pdf |

### 2018

|   Month     | Original Filename                     | Base Path         |
|-------------|---------------------------------------|-------------------|
| January     | Coles-January-2018.pdf                | Jan%202018%20Coles%20Mag_u318ns02.pdf |
| February    | Coles - February 2018 - Offer valid Mon 8 Feb - Thu 31 Dec 2030.pdf | Coles_Feb18_Mag_83h0u62d1.pdf |
| March       | ColesMag_March2018_y7j0l3nd7.pdf      | ColesMag_March2018_y7j0l3nd7.pdf |
| April       | ColesMag_April2018_f3j59djwa.pdf      | ColesMag_April2018_f3j59djwa.pdf |
| May         | Cole May 2018 Magazine_u7g3m9sk2.pdf  | Cole%20May%202018%20Magazine_u7g3m9sk2.pdf |
| June        | ColesMag_Jun2018_JFEISA3JSF.pdf       | ColesMag_Jun2018_JFEISA3JSF.pdf |

## Woolworths

* Base URL: 
http://d2g5na3xotdfpc.cloudfront.net/uploads/pdf/
### 2009

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| December    | 6107_Fresh-Mag-Dec-2009.pdf   | 6107_Fresh-Mag-Dec-2009.pdf |
| November    | 6108_Fresh-Mag-Nov-2009.pdf   | 6108_Fresh-Mag-Nov-2009.pdf |
| October     | 6109_Fresh-Mag-Oct-2009.pdf   | 6109_Fresh-Mag-Oct-2009.pdf |
| September   | 6110_Fresh-Mag-Sep 2009.pdf   | 6110_Fresh-Mag-Sep%202009.pdf |
| August      | 6111_Fresh-Mag-Aug-2009.pdf   | 6111_Fresh-Mag-Aug-2009.pdf |
| July        | 6112_Fresh-Mag-July-2009.pdf  | 6112_Fresh-Mag-July-2009.pdf |
| June        | 6113_Fresh-Mag-June-2009.pdf  | 6113_Fresh-Mag-June-2009.pdf |
| May         | 6114_Fresh-Mag-May-2009.pdf   | 6114_Fresh-Mag-May-2009.pdf |
| April       | 6115_Fresh-Mag-April-2009.pdf | 6115_Fresh-Mag-April-2009.pdf |
| March       | 6116_Fresh-Mag-March-2009.pdf | 6116_Fresh-Mag-March-2009.pdf |
| February    | 6117_Fresh-Mag-Feb-2009.pdf   | 6117_Fresh-Mag-Feb-2009.pdf |
| January     | 6118_Fresh-Mag-Jan-2009.pdf   | 6118_Fresh-Mag-Jan-2009.pdf |

### 2010

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January     | 6026_Fresh-Mag-Jan-2010.pdf   | 6026_Fresh-Mag-Jan-2010.pdf |
| February    | 6027_Fresh-Mag-Feb-2010.pdf   | 6027_Fresh-Mag-Feb-2010.pdf |
| March       | 6030_Fresh-Mag-March-2010.pdf | 6030_Fresh-Mag-March-2010.pdf |
| April       | 6031_Fresh-Mag-April-2010.pdf | 6031_Fresh-Mag-April-2010.pdf |
| May         | 6029_Fresh-Mag-May-2010.pdf   | 6029_Fresh-Mag-May-2010.pdf |
| June        | 6032_Fresh-Mag-June-2010.pdf  | 6032_Fresh-Mag-June-2010.pdf |
| July        | 6033_Fresh-Mag-July-2010.pdf  | 6033_Fresh-Mag-July-2010.pdf |
| August      | 6034_Fresh-Mag-Aug-2010.pdf   | 6034_Fresh-Mag-Aug-2010.pdf |
| September   | 6035_Fresh-Mag-Sep-2010.pdf   | 6035_Fresh-Mag-Sep-2010.pdf |
| October     | 6036_Fresh-Mag-Oct-2010.pdf   | 6036_Fresh-Mag-Oct-2010.pdf |
| November    | 6037_Fresh-Mag-Nov-2010.pdf   | 6037_Fresh-Mag-Nov-2010.pdf |
| December    | 6038_Fresh-Mag-Dec-2010.pdf   | 6038_Fresh-Mag-Dec-2010.pdf |

### 2011

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January     | 5978_Fresh-Mag-Jan-2011.pdf   | 5978_Fresh-Mag-Jan-2011.pdf |
| February    | 5977_Fresh-Mag-Feb-2011.pdf   | 5977_Fresh-Mag-Feb-2011.pdf |
| March       | 5976_Fresh-Mag-March-2012.pdf | 5976_Fresh-Mag-March-2012.pdf |
| April       | 5975_Fresh-Mag-April-2011.pdf | 5975_Fresh-Mag-April-2011.pdf |
| May         | 5979_Fresh-Mag-May-2011.pdf   | 5979_Fresh-Mag-May-2011.pdf |
| June        | 5974_Fresh-Mag-June-2011.pdf  | 5974_Fresh-Mag-June-2011.pdf |
| July        | 5973_Fresh-Mag-July-2011.pdf  | 5973_Fresh-Mag-July-2011.pdf |
| August      | 5972_Fresh-Mag-Aug-2011.pdf   | 5972_Fresh-Mag-Aug-2011.pdf |
| September   | 5971_FreshMag-Sep-2011.pdf    | 5971_FreshMag-Sep-2011.pdf |
| October     | 5970_Fresh-Mag-Oct-2011.pdf   | 5970_Fresh-Mag-Oct-2011.pdf |
| November    | 5969_Fresh-Mag-Nov-2011.pdf   | 5969_Fresh-Mag-Nov-2011.pdf |
| December    | 5967_Fresh-Mag-Dec-2011.pdf   | 5969_Fresh-Mag-Nov-2011.pdf |

### 2012

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January     | 5931_Fresh-Mag-Jan-2012.pdf   | 5931_Fresh-Mag-Jan-2012.pdf |
| February    | 5930_Fresh-Mag-Feb-2012.pdf   | 5930_Fresh-Mag-Feb-2012.pdf |
| March       | 5929_Fresh-Mag-March-2012.pdf | 5929_Fresh-Mag-March-2012.pdf |
| April       | 5928_Fresh-Mag-April-2012.pdf | 5928_Fresh-Mag-April-2012.pdf |
| May         | 5927_Fresh-Mag-May-2012.pdf   | 5927_Fresh-Mag-May-2012.pdf |
| June        | 5926_Fresh-Mag-June-2012.pdf  | 5926_Fresh-Mag-June-2012.pdf |
| July        | 5925_Fresh-Mag-July-2012.pdf  | 5925_Fresh-Mag-July-2012.pdf |
| August      | 5923_Fresh-Mag-Aug-2012.pdf   | 5923_Fresh-Mag-Aug-2012.pdf |
| September   | 5924_Fresh-Mag-Sep-2012.pdf   | 5924_Fresh-Mag-Sep-2012.pdf |
| October     | 5922_Fresh-Mag-Oct-2012.pdf   | 5922_Fresh-Mag-Oct-2012.pdf |
| November    | 5921_Fresh-Mag-Nov-2012.pdf   | 5921_Fresh-Mag-Nov-2012.pdf |
| December    | 5920_Fresh-Mag-Dec-2012.pdf   | 5920_Fresh-Mag-Dec-2012.pdf |

### 2013

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| June        | 5326_Fresh-Mag-June-2013C.pdf | 5326_Fresh-Mag-June-2013C.pdf |

### 2014

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| April       | 6121_Fresh-Mag-April-2014.pdf | 6121_Fresh-Mag-April-2014.pdf |
| May         | 6318_Fresh Mag May 2014.pdf   | 6318_Fresh%20Mag%20May%202014.pdf |
| June        | 6464_Fresh Mag June 2014.pdf  | 6464_Fresh%20Mag%20June%202014.pdf |
| July        | 6680_July_Fresh_Combined.pdf  | 6680_July_Fresh_Combined.pdf |
| August      | 6830_AugFreshCOMB.pdf         | 6830_AugFreshCOMB.pdf |
| September   | 7017_September_combined.pdf   | 7017_September_combined.pdf |
| October     | 6121_Fresh-Mag-Oct-2014.pdf   | Oct_pdf2.pdf |
| November    | November16Oct2014.pdf         | November16Oct2014.pdf |
| December    | Binder1.pdf                   | Binder1.pdf |

### 2015

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January/February  | FreshJan2015_Gh3rV@.pdf | FreshJan2015_Gh3rV@.pdf |
| March       | FreshMarch2015.pdf            | FRE0315_df4D.pdf |
| April       | AprilFreshMag_jG2p.pdf        | AprilFreshMag_jG2p.pdf |
| May         | MayFresh_210415_hF3d.pdf      | MayFresh_210415_hF3d.pdf |
| June        | JuneFresh_110615_he5E.pdf     | JuneFresh_110615_he5E.pdf |
| July        | FreshJuly_3006115_jW9d.pdf    | FreshJuly_3006115_jW9d.pdf |
| August      | AugFreshMag_jD9f.pdf          | AugFreshMag_jD9f.pdf |
| September   | WWfresh_Sept_170815_n4Cu.pdf  | WWfresh_Sept_170815_n4Cu.pdf |
| October     | Oct_270915_jf3G.pdf           | Oct_270915_jf3G.pdf |
| November    | WW_NovFresh15_h7D3.pdf        | WW_NovFresh15_h7D3.pdf |
| December    | DecFresh2015_h3pf.pdf         | DecFresh2015_h3pf.pdf |

### 2016

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January/February | JanuaryFresh2016_h8m2.pdf  | JanuaryFresh2016_h8m2.pdf |
| March       | MarchFresh2016_n9l2.pdf         | MarchFresh2016_n9l2.pdf |
| April       | AprilFresh2016_p4j9.pdf         | AprilFresh2016_p4j9.pdf |
| May         | MayFresh2016_u3h0.pdf           | MayFresh2016_u3h0.pdf |
| June        | JuneFresh2016_f8g5.pdf          | JuneFresh2016_f8g5.pdf |
| July        | July_Fresh_2016.pdf             | July_Fresh_2016.pdf |
| August      | WW_August2016_FreshMag.pdf      | WW_August2016_FreshMag.pdf |
| September   | WW-Mag_Sept2016_n4h8.pdf        | WW-Mag_Sept2016_n4h8.pdf |
| October     | WW_Oct2016_FreshMag.pdf         | WW_Oct2016_FreshMag.pdf |
| November    | WW_FreshMag_Nov2016.pdf         | WW_FreshMag_Nov2016.pdf |
| December    | WW_FreshMag_Dec2016_k6l4.pdf    | WW_FreshMag_Dec2016_k6l4.pdf |

### 2017

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January/February | WW_Jan2017_Mag_9DSH3K.pdf      | WW_Jan2017_Mag_9DSH3K.pdf |
| March       | WW_FreshMag_March2017_h3j22.pdf     | WW_FreshMag_March2017_h3j22.pdf |
| April       | WW_Mag_April_2017_s93ks.pdf         | WW_Mag_April_2017_s93ks.pdf |
| May         | WW_May 2017 Fresh Mag_sk83ssf.pdf   | WW_May%202017%20Fresh%20Mag_sk83ssf.pdf |
| June        | WW_June 2017 Fresh Mag_j93ls.pdf    | WW_June%202017%20Fresh%20Mag_j93ls.pdf |
| July        | WW_FreshMag_July2017_j7h6d.pdf      | WW_FreshMag_July2017_j7h6d.pdf |
| August      | WW_Aug2017_FreshMag_J7G6S160.pdf    | WW_Aug2017_FreshMag_J7G6S160.pdf |
| September   | WW Sept 2017 Fresh Mag_u7j39.pdf    | WW%20Sept%202017%20Fresh%20Mag_u7j39.pdf |
| October     | WW_October2017_FreshMag_83jdiw.pdf  | WW_October2017_FreshMag_83jdiw.pdf |
| November    | WW_November2017_FreshMag_JSK8S4.pdf | WW_November2017_FreshMag_JSK8S4.pdf |
| December    | WW_FreshMag_Dec2017_nv39kej3.pdf    | WW_FreshMag_Dec2017_nv39kej3.pdf |

### 2018

| Month       | Original Filename             | Base Path         |
|-------------|-------------------------------|-------------------|
| January/February | WW_Jan-Feb2018 FreshMag_j38hdk3e.pdf | WW_Jan-Feb2018%20FreshMag_j38hdk3e.pdf |
| March       | WW_March2018_FreshMag_k8j62en3.pdf        | WW_March2018_FreshMag_k8j62en3.pdf |
| April       | WW_April2018_FreshCrossBuns_aj3j8sk.pdf   | WW_April2018_FreshCrossBuns_aj3j8sk.pdf |
| May         | WW_May2018_FreshMag_j8b3m9d1.pdf          | WW_May2018_FreshMag_j8b3m9d1.pdf |
| June        | WW_Jun2018_FreshMag_jai23iakfw.pdf        | WW_Jun2018_FreshMag_jai23iakfw.pdf |
| July        | WW_July2018_FreshMag_RY476HXS.pdf         | WW_July2018_FreshMag_RY476HXS.pdf |
